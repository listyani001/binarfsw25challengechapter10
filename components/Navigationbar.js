import style from '../assets/styles/NavigationBar.module.css';
import Link from 'next/link';
import MyScore from './MyScore';
import { selectAuthState, setAuthState } from "/store/authSlice";
import { AuthState } from "/store/authSlice";
import { useDispatch, useSelector } from "react-redux";

export default function NavigationBar() {
  return (
    <>
    const AuthState = useAuthState();
    const dispatch = useDispatch();
      <header className={style.header}>
        <div className={style.container}>
          <nav className={style.nav}>
            <ul className={style.ul}>
              {/* NavBar Link */}
              <li className={style.li}>
                <Link className={style.a} href="/">
                  LandingPage
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/gamelist">
                  GameList
                </Link>
              </li>

               <li className={style.li}>
                <Link className={style.a} href="/game">
                  Game
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/gamedetail">
                  GameDetail
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/story">
                  Story
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/userprofile">
                  UserProfile
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/updateuser">
                  ProfileUpdate
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/leaderboard">
                  Leaderboard
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/newgame">
                  NewGame
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/currentinfo">
                  CurrentInfo
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/dummy_games">
                  DummyGames
                </Link>
              </li>

              <li className={style.li}>
                <Link className={style.a} href="/">
                  Logout
                </Link>
              </li>

            </ul>
            <MyScore />
          </nav>
        </div>
      </header>
      <div style={{ color:"white", backgroundColor:"black" }}>{AuthState ? "Not Logged in" : " Logged In"}</div>
      {/* NavBar Dropdown */}
      {/* <div className={style.dropdown}>
            <button type="button" className={style.dropbtn}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="12"
                height="12"
                fill="currentColor"
                className="bi bi-caret-down-fill"
                viewBox="0 0 16 16"
                style={{ marginRight: "5px" }}
              >
                <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z" />
              </svg>
              Dropdown
            </button>
            <div className={style.dropdown_content}>
              <a href="/about">Link 1</a>
              <a href="#">Link 2</a>
              <a href="#">Link 3</a>
            </div>
          </div> */}

      {/* Log In & Register */}
      {/* <li id={style.navbar}>
            <button className={style.button_24} role="button" id={style.login}>
              Log In
            </button>
          </li>
          <li id={style.navbar}>
            <button className={style.button_24} role="button">
              Register
            </button>
          </li> */}
    </>
  );
}
