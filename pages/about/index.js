import style from "../../assets/styles/About.module.css";
import Footer from "../../components/Footer";
import NavigationBar from "../../components/Navigationbar";

export default function about() {
  return (
    <>
      <NavigationBar />
      <div className={style.about}>
        <h1>Team Role</h1>
        <table className={style.table}>
          <tbody>
            <tr>
              <td className={style.ddot}>Repo Maintainer</td>
              <td className={style.ddot}>:</td>
              <td className={style.ddot}>Fauzi Fauzi</td>
            </tr>
            <tr>
              <td className={style.ddot}>Repo Maintainer</td>
              <td className={style.ddot}>:</td>
              <td className={style.ddot}>M Fauzan Zidny</td>
            </tr>
            <tr>
              <td className={style.ddot}>Boiler Coder</td>
              <td className={style.ddot}>:</td>
              <td className={style.ddot}>M Akmal F</td>
            </tr>
            <tr>
              <td className={style.ddot}>Boiler Coder</td>
              <td className={style.ddot}>:</td>
              <td className={style.ddot}>Suhairoh</td>
            </tr>
            <tr>
              <td className={style.ddot}>Dev Ops</td>
              <td className={style.ddot}>:</td>
              <td className={style.ddot}>Andri Anggoro</td>
            </tr>
            <tr>
              <td className={style.ddot}>Scrum Master</td>
              <td className={style.ddot}>:</td>
              <td className={style.ddot}>Sylvia</td>
            </tr>
          </tbody>
        </table>
      </div>
      <Footer />
    </>
  );
}
