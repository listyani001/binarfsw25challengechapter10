import { useState } from 'react';
import Swal from 'sweetalert2';
import { auth, firebaseInit } from '../../config/firebaseInit';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { useRouter } from 'next/router';
// import Link from 'next/link';
import NavBar from '/components/Navigationbar';
import { getDatabase, ref, set, push } from 'firebase/database';
// import { last } from 'lodash';

const login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  //   const [uid, setUid] = useState('');
  //   const [first_name, setFirstName] = useState('');
  //   const [last_name, setLastName] = useState('');
  //   const [datebirth, setDateBirth] = useState('');
  //   const [gender, setGender] = useState('');
  const router = useRouter();

  const writeUserData = (email, password) => {
    const db = getDatabase(firebaseInit);
    const param = {
      first_name: first_name,
      last_name: last_name,
      email: email,
      uid: userId,
      gender: gender,
      datebirth: datebirth,
      skor: 0,
    };
    console.log(param, 'param');
    set(ref(db, 'users/' + userId), param);
  };
  const setTglLahir = (value) => {
    console.log(value);
    setDateBirth(value);
  };
  const loginHandler = async (e) => {
    console.log('login');
    e.preventDefault();

    // return;
    signInWithEmailAndPassword(auth, email, password)
      .then((hsl) => {
        console.log(hsl);
        // setUid(hsl.user.uid);
        // writeUserData(hsl.user.uid, first_name, last_name, email, datebirth, gender);
        Swal.fire({
          icon: 'success',
          text: 'login berhasil',
        }).then((hsl) => {
          router.push('/');
        });
      })
      .catch((err) => {
        console.log(err);
        Swal.fire({
          icon: 'error',
          text: 'login gagal ' + err.message,
        });
      });
  };
  return (
    <>
      <NavBar />
      <section className="vw-100 vh-100" style={{ background: 'purple' }}>
        <div className="container d-flex justify-content-center align-items-center">
          <form>
            <h3 className="text-center md-3 p-5" style={{ fontFamily: 'arial black', color: `plum` }}>
              Sign in
            </h3>
            <div className="mb-3" style={{ color: `purple` }}>
              <label className="form-label" style={{ color: 'white' }}>
                Email address: :
              </label>
              <input type="email" className="form-control" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required />
            </div>
            <div className="mb-3" style={{ color: `purple` }}>
              <label className="form-label" style={{ color: 'white' }}>
                Password: :
              </label>
              <input type="password" className="form-control" placeholder="Enter password" value={password} onChange={(e) => setPassword(e.target.value)} required />
            </div>
            <div className="text-center md-3 p-4">
              <button type="submit" className="btn btn-primary" style={{ backgroundColor: `plum` }} onClick={loginHandler}>
                Sign in
              </button>
            </div>
          </form>
        </div>
      </section>
    </>
  );
};

export default login;
