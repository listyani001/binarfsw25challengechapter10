import { useState } from 'react';
import Swal from 'sweetalert2';
import { auth, firebaseInit } from '../../config/firebaseInit';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { useRouter } from 'next/router';
import NavBar from '/components/Navigationbar';
import { getDatabase, ref, set, push } from 'firebase/database';
import { useEffect, useState, useRef } from 'react';

const score = () => {
  const [score, setscore] = useState('');
  const router = useRouter();

  return (
    <div className="row">
      <div className="form-group col-md-12">
        <label htmlFor="name">Skor</label>
        <input type="skor" readOnly className="form-control" id="skor" required placeholder="" value={skor} />
      </div>
    </div>
  );
};

export default score;
